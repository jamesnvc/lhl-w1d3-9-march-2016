//
//  main.m
//  OOPDemo
//
//  Created by James Cash on 09-03-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Calculator.h"
#import "AdSupportedCalculator.h"
#import "VerboseAdCalculator.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Calculator *calc = [[Calculator alloc] init];
        NSInteger someVal = 5;
        NSLog(@"%ld + meaninng = %ld", someVal, [calc addMeaningTo:someVal]);

        AdSupportedCalculator *adCalc = [[AdSupportedCalculator alloc] init];
        NSLog(@"%ld + meaninng = %ld", someVal, [adCalc addMeaningTo:someVal]);

        VerboseAdCalculator *verbCalc = [[VerboseAdCalculator alloc] initWithMeaning:5];
        NSLog(@"%ld + meaninng = %ld", someVal, [verbCalc addMeaningTo:someVal]);

        NSLog(@"Meaning of %@ is %ld", calc, [calc meaningOf:calc]);
        Calculator *calc2 = nil;
        NSLog(@"Meaning of %@ is %ld", calc2, [calc meaningOf:calc2]);

    }
    return 0;
}
