//
//  VerboseAdCalculator.m
//  OOPDemo
//
//  Created by James Cash on 09-03-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "VerboseAdCalculator.h"

@implementation VerboseAdCalculator

- (NSInteger)addMeaningTo:(NSInteger)x
{
    NSLog(@"Start adding meaning");
    NSInteger result = [super addMeaningTo:x];
    NSLog(@"Meaning is %ld", result);
    return result;
}

@end
