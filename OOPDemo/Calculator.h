//
//  MathThing.h
//  OOPDemo
//
//  Created by James Cash on 09-03-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Calculator : NSObject

@property (assign) NSInteger meaning;

- (instancetype)initWithMeaning:(NSInteger)meaning;

- (NSInteger)addMeaningTo:(NSInteger)x;
- (NSInteger)meaningOf:(Calculator*)calc;
@end
