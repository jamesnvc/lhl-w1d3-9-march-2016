//
//  MathThing.m
//  OOPDemo
//
//  Created by James Cash on 09-03-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "Calculator.h"

@implementation Calculator

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.meaning = 42;
    }
    return self;
}

- (instancetype)initWithMeaning:(NSInteger)meaning
{
    self = [super init];
    if (self) {
        self.meaning = meaning;
    }
    return self;
}

- (NSInteger)addMeaningTo:(NSInteger)x
{
    return x + self.meaning;
}

- (NSInteger)meaningOf:(Calculator *)calc
{
    return calc.meaning;
}

@end
