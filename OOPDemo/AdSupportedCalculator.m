//
//  AdSupportedCalculator.m
//  OOPDemo
//
//  Created by James Cash on 09-03-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "AdSupportedCalculator.h"

@interface AdSupportedCalculator ()

@property (nonatomic,strong) NSString *sponsor;

@end

@implementation AdSupportedCalculator

- (NSInteger)addMeaningTo:(NSInteger)x
{
    if (self.sponsor != nil) {
        NSLog(@"This addition brought to you by our sponsor %@!", self.sponsor);
    } else {
        NSLog(@"Your ad here");
    }
    return [super addMeaningTo:x*2];
}

@end
